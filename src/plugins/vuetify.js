import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors';

Vue.use(Vuetify)

const opts = {
    theme: {
        primary: colors.indigo.base, // #E53935
        secondary: colors.indigo.base, // #FFCDD2
        accent: colors.indigo.base // #3F51B5
    }
}

export default new Vuetify(opts)
