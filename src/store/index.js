import Vue from 'vue';
import Vuex from 'vuex';

import base from './modules/base';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({

    linkActiveClass: 'active',
    mode: 'hash',
    modules: {
        base,
        user,
    },
    strict: process.env.NODE_ENV !== 'production'
});