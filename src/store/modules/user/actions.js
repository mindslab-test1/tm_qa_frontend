import 'url-search-params-polyfill';
import { RepositoryFactory } from '@/repositories/RepositoryFactory';
import router from '@/routes/index';
import store from "@/store";

const userRepository = RepositoryFactory.get('user');

const signIn =  ({ commit }, loginObj) => { 
    let params = {
        'userId': loginObj.userId,
        'userPw': loginObj.userPw,
    };
    return new Promise((resolve, reject) => {
        userRepository.sign_in(params).then((res) => {
            let return_obj = {
                'status': res.status,
                'rMsg': '',
            }
            if(res.status == 200) {
                if(res.data.firstLogin){
                    return resolve(res.data.user);
                }else{
                    let user_info = res.data.user_info
                    let {access_token, refresh_token} = res.data;
                    commit('setUserToken', {access_token, refresh_token});
                    commit('setUserInfo', user_info);
                    return resolve();
                }
            }else if(res.status == 401) {
                return_obj.rMsg = "아이디 또는 비밀번호를 다시 확인해주세요.";
                return reject(return_obj);
            }else if(res.status == 403){
                return_obj.rMsg = "5회 이상 로그인에 실패하여 계정이 잠금처리 되었습니다.\n관리자에게 문의 바랍니다.";
                return reject(return_obj);
            }else {
                return_obj.rMsg = "로그인 실패(500:시스템 오류 발생)";
                return reject(return_obj);
            }
        }).catch((error) => {
            return_obj.rMsg = `로그인 실패("${error.response.status}")`;
            return reject(return_obj);
        })
    });
};

/* const signOut =  ({ commit, state, getters }) => {

    let userNo = state.userInfo.userNo;
    let refresh_token = getters.getRefreshTokenFromLocalStorage();
    
    return new Promise((resolve, reject) => {
        if(!refresh_token) {
            commit('clearUserInfo')
            return resolve("로그아웃 처리 되었습니다.");
        }else {
            let params = { 'userNo': userNo, 'refresh_token' : refresh_token };
            userRepository.sign_out(params).then((res) => {
                commit('clearUserInfo')
                return resolve(res.data.processMessage);
            }).catch((error) => {
                return reject("로그아웃 실패(" + error.response.status + ")");
            });
        }
    });
}; */

const signOut =  ({ commit, state, getters }) => {
    return new Promise((resolve, reject) => {
        userRepository.sign_out().then((res) => {
            commit('clearUserInfo');
            commit('clearTokenInterval');
            return resolve(res.data.processMessage);
        }).catch((error) => {
            return reject("로그아웃 실패(" + error.response.status + ")");
        });
    });
}

const signUp =  ({ commit }, signUpObj) => {
    return new Promise((resolve, reject) => {
        userRepository.sign_up(signUpObj).then((res) => {
            const data = res.data;
            if(data.processStatus == "SIGNUP_DUPLICATE") {
                return reject(data.processMessage)
            }
            commit('clearUserInfo')
            return resolve(data.processMessage);
        }).catch((error) => {
            return reject("회원가입 실패(" + error.response.status + ")");
        });
    });
};

const checkTokenTimeOut = ({commit, state, dispatch}, time) => {
    commit('setTokenTimeOut', time/1000);
    commit('setTokenInterval', setInterval(() => {
        commit('setTokenTimeOut', state.tokenTimeOut-1);
        if(state.tokenTimeOut === 60){
            store.commit('base/setModalStates', [{name: 'TimeOutModal', parameters: {msg: '아무런 동작이 없을 경우 1분 후 세션이 종료됩니다.'}}]);
        }
        if(state.tokenTimeOut === 0){
            clearInterval(state.tokenInterval);
            dispatch('signOut').then(r => {
                store.commit('base/setModalStates', [{name: 'TimeOutModal', parameters: {msg: '로그인 유지 시간이 만료되어 로그아웃 처리 되었습니다.'}}]);
                router.push('/');
            });
        }
    }, 1000));
}

export default {
    signIn,
    signOut,
    signUp,
    checkTokenTimeOut,
};