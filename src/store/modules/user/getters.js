const getRefreshTokenFromLocalStorage = () => ()  =>{
    return localStorage.getItem("refresh_token") || '';
};

const getUserInfo = (state) => state.userInfo || {};

const getTokenTimeOut = (state) => state.tokenTimeOut;

const getTokenInterval = (state) => state.tokenInterval;

export default {
    getRefreshTokenFromLocalStorage,
    getUserInfo,
    getTokenTimeOut,
    getTokenInterval,
};