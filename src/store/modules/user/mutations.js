const setUserInfo = (state, userInfo) => {
    state.userInfo = userInfo || {}
};

const setUserToken = (state, {access_token, refresh_token}) => {
    if(access_token) {
        localStorage.setItem("access_token", access_token);
    }
    if(refresh_token) {
        localStorage.setItem("refresh_token", refresh_token);
    }
    
};

const setTokenTimeOut = (state, time) => {
    state.tokenTimeOut = time;
};

const clearUserInfo = (state) => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    localStorage.removeItem("user");
    state.userInfo = {};
};

const setTokenInterval = (state, tokenInterval) => {
    state.tokenInterval = tokenInterval;
}

const clearTokenInterval = (state) => {
    clearInterval(state.tokenInterval);
}

export default {
    setUserInfo,
    setUserToken,
    setTokenTimeOut,
    setTokenInterval,
    clearTokenInterval,
    clearUserInfo,
};