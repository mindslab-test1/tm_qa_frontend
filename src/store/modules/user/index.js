import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    userInfo: {},
    tokenTimeOut: 0,
    tokenInterval: null,
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};