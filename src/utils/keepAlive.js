import {EventBus} from "./bus";

function isDef (v) {
    return v !== undefined && v !== null
}

function isAsyncPlaceholder (node) {
    return node.isComment && node.asyncFactory
}

function getFirstComponentChild (children) {
    if (Array.isArray(children)) {
        for (var i = 0; i < children.length; i++) {
            var c = children[i];
            if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
                return c
            }
        }
    }
}

const _toString = Object.prototype.toString
function isRegExp(v) {
    return _toString.call(v) === '[object RegExp]'
}
export function remove(arr, item) {
    if (arr.length) {
        const index = arr.indexOf(item)
        if (index > -1) {
            return arr.splice(index, 1)
        }
    }
}

function getComponentName (opts) {
    return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
    if (Array.isArray(pattern)) {
        return pattern.indexOf(name) > -1
    } else if (typeof pattern === 'string') {
        return pattern.split(',').indexOf(name) > -1
    } else if (isRegExp(pattern)) {
        return pattern.test(name)
    }
    /* istanbul ignore next */
    return false
}

function pruneCache (keepAliveInstance, filter) {
    var cache = keepAliveInstance.cache;
    var keys = keepAliveInstance.keys;
    var _vnode = keepAliveInstance._vnode;
    for (var key in cache) {
        var entry = cache[key];
        if (entry) {
            var name = entry.name;
            if (name && !filter(name)) {
                pruneCacheEntry(cache, key, keys, _vnode);
            }
        }
    }
}

function pruneCacheEntry (
    cache,
    key,
    keys,
    current
) {
    var entry = cache[key];
    if (entry && (!current || entry.tag !== current.tag)) {
        entry.componentInstance.$destroy();
    }
    cache[key] = null;
    remove(keys, key);
}

function removeCacheByKey (
    cache,
    keys,
    keyToCache,
    current
) {
    var key = Object.keys(cache).find(v => v.split('-')[0] === keyToCache.split('-')[0]);
    var entry = cache[key];
    if (entry && (!current || entry.tag !== current.tag)) {
        entry.componentInstance.$destroy();
    }
    delete cache[key];
    remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

export default {
    name: 'keep-alive',
    abstract: true,

    props: {
        include: patternTypes,
        exclude: patternTypes,
        max: [String, Number]
    },

    methods: {
        cacheVNode: function cacheVNode() {
            var ref = this;
            var cache = ref.cache;
            var keys = ref.keys;
            var vnodeToCache = ref.vnodeToCache;
            var keyToCache = ref.keyToCache;
            if (vnodeToCache) {
                var tag = vnodeToCache.tag;
                var componentInstance = vnodeToCache.componentInstance;
                var componentOptions = vnodeToCache.componentOptions;
                if(cache[Object.keys(cache).find(v => v.split('-')[0] === keyToCache.split('-')[0])]){
                    removeCacheByKey(cache, keys, keyToCache, this._vnode);
                }
                cache[keyToCache] = {
                    name: getComponentName(componentOptions),
                    tag: tag,
                    componentInstance: componentInstance,
                };
                keys.push(keyToCache);
                // prune oldest entry
                if (this.max && keys.length > parseInt(this.max)) {
                    pruneCacheEntry(cache, keys[0], keys, this._vnode);
                }
                this.vnodeToCache = null;
            }
        },
        clearCache: function (){
            this.cache = {};
        },
        removeCacheByKey: function(key){
            var ref = this;
            removeCacheByKey(ref.cache, ref.keys, key, this._vnode);
        }
    },

    created: function created () {
        this.cache = Object.create(null);
        this.keys = [];
        EventBus.$on('clearCache', this.clearCache);
        EventBus.$on('removeCacheByKey', this.removeCacheByKey);
    },

    destroyed: function destroyed () {
        for (var key in this.cache) {
            pruneCacheEntry(this.cache, key, this.keys);
        }
    },

    mounted: function mounted () {
        var this$1 = this;

        this.cacheVNode();
        this.$watch('include', function (val) {
            pruneCache(this$1, function (name) { return matches(val, name); });
        });
        this.$watch('exclude', function (val) {
            pruneCache(this$1, function (name) { return !matches(val, name); });
        });
    },

    updated: function updated () {
        this.cacheVNode();
    },

    render: function render () {
        var slot = this.$slots.default;
        var vnode = getFirstComponentChild(slot);
        var componentOptions = vnode && vnode.componentOptions;
        if (componentOptions) {
            // check pattern
            var name = getComponentName(componentOptions);
            var ref = this;
            var include = ref.include;
            var exclude = ref.exclude;
            if (
                // not included
                (include && (!name || !matches(include, name))) ||
                // excluded
                (exclude && name && matches(exclude, name))
            ) {
                return vnode
            }

            var ref$1 = this;
            var cache = ref$1.cache;
            var keys = ref$1.keys;
            var key = vnode.key == null
                // same constructor may get registered as different local components
                // so cid alone is not enough (#3269)
                ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
                : vnode.key;
            if (cache[key]) {
                vnode.componentInstance = cache[key].componentInstance;
                // make current key freshest
                remove(keys, key);
                keys.push(key);
            } else {
                // delay setting the cache until update
                this.vnodeToCache = vnode;
                this.keyToCache = key;
            }

            vnode.data.keepAlive = true;
        }
        return vnode || (slot && slot[0])
    }
};
