import XLSX from "xlsx";

const dataTableToXLSX = (header, data, sheetName, fileName) => {
    let h;
    if(Array.isArray(header)) {
        h = header.reduce((a, b) => {
			return a[b.datafield] = b.text, a;
        }, {});
    }else {
        h = header;
    }
    
    let excelData = data.map((itm) => {
        let tmpObj = {};
        for (let column of Object.keys(itm)) {
            if (Object.keys(h).includes(column)) {
                tmpObj[column] = itm[column];
            }
        }
        return tmpObj;
    })

    let copiedData = _.cloneDeep(excelData);
    copiedData.unshift(h);
    const worksheet = XLSX.utils.json_to_sheet(copiedData, { skipHeader: true });
    let workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, sheetName);
    XLSX.writeFile(workbook, `${fileName}.xlsx`);
};

export default {
    dataTableToXLSX
}