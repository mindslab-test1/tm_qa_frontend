import "chartjs-chart-box-and-violin-plot";
import { generateChart, mixins } from "vue-chartjs";
const { reactiveProp } = mixins;

const Pie = generateChart("pie", "pie");

export default {
    extends: Pie,
    mixins: [reactiveProp],
    props: ["options"],
    mounted() {
        this.renderChart(this.chartData, this.options);
    }
};