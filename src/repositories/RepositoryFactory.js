import UserRepository from './userRepository';
import CodeRepository from './codeRepository';
import HomeRepository from './homeRepository';
import TmSttTargetRepository from './tmSttTargetRepository'
import QaTargetRepository from './qaTargetRepository';
import QaAssignRepository from './qaAssignRepository';
import QaResultRepository from './qaResultRepository';
import QaAuditRepository from './qaAuditRepository';
import QaSttAllRepository from './qaSttAllRepository';
import StatisticsRepository from "./statisticsRepository";
import MonitoringRepository from './monitoringRepository';
import ManageRepository from './manageRepository';
import RealtimeRepository from "./RealtimeRepository";


const repositories = {
  code: CodeRepository,
  realtime: RealtimeRepository,
  user: UserRepository,
  home: HomeRepository,
  sttTarget: TmSttTargetRepository,
  qaTarget: QaTargetRepository,
  qaAssign: QaAssignRepository,
  qaResult: QaResultRepository,
  statistics: StatisticsRepository,
  monitoring: MonitoringRepository,
  qaAudit: QaAuditRepository,
  qaSttAll: QaSttAllRepository,
  manage: ManageRepository,
  // other repositories ...
};

export const RepositoryFactory = {
  get: (name) => repositories[name],
};
