import Repository from "./Repository";

const resource = "/tm/stt";

export default {
    getSttTargetList(data) {
        return Repository.get(`${resource}/target/list?${data}`);
    },
	getProgStatCdList() {
        return Repository.get(`${resource}/target/code`);
    },
};