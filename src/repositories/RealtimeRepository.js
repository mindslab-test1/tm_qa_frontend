import Repository from "./Repository";

const resource = "/real-time";

export default {
    connect() {
        return Repository.get(`${resource}`);
    },
    connectStt(param) {
        return Repository.post(`${resource}/stt`, param);
    },
};