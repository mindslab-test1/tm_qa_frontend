import Repository from "./Repository";

const resource = "/tmqa/assign";

export default {
    getQaAssignTargetList(data) {
        return Repository.post(`${resource}/target/list`, data);
    },
    getAssignCntList(data) {
        return Repository.post(`${resource}/user/reviewer/assign/list`, data);
    },
    getQaUserList() {
        return Repository.post(`${resource}/user/reviewer/list`);
    },
    setOrUpdHolidayInfo(data) {
        return Repository.post(`${resource}/holiday`, data);
    },
    setAssignInfo(data) {
        return Repository.post(`${resource}/info`, data);
    },
    getPerJudgeDetail(data) {
        return Repository.post(`${resource}/judge/detail`, data);
    },
};