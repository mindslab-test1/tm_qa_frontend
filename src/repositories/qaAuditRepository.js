import Repository from "./Repository";

const resource = "/tmqa/audit";

export default {
    getQaAuditMasterInfo(nsplNo) {
        return Repository.get(`${resource}/offer/${nsplNo}`);
    },
    getQaAuditCallList(nsplNo) {
        return Repository.get(`${resource}/stts/${nsplNo}`);
    },
    getQaAuditRoundList(nsplNo) {
        return Repository.get(`${resource}/rounds/${nsplNo}`);
    },
    getEvaluationItemList(nsplNo, tmsInfo, psnJugCat, chkCtgCd) {
        return Repository.get(`${resource}/contract/${nsplNo}/evaluation-item/${tmsInfo}-${psnJugCat}-${chkCtgCd}`);
    },
    getContractScriptList(nsplNo, tmsInfo, prodCat, chkItmCd) {
        return Repository.get(`${resource}/contract/${nsplNo}/script/${tmsInfo}-${prodCat}-${chkItmCd}`);
    },
    getContractScriptDetectionList(nsplNo, tmsInfo, prodCat, chkItmCd) {
        return Repository.get(`${resource}/contract/${nsplNo}/script/detection/${tmsInfo}-${prodCat}-${chkItmCd}`);
    },
    setAudit(param) {
        return Repository.put(`${resource}/round`, param);
    },
    getQuestionsAndAnswers(nsplNo) {
        return Repository.get(`${resource}/qna/${nsplNo}`);
    },
    setAnswer(param) {
        return Repository.post(`${resource}/qna/answer`, param);
    },
};