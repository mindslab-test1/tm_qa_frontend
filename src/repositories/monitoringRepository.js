import Repository from "./Repository";

const resource = "/monitoring";

export default {
    getSttMainList(data) {
        return Repository.post(`${resource}/getSttMainList`, data);
    },
    getTaMainList(data) {
        return Repository.post(`${resource}/getTaMainList`, data);
    },
    getServiceMntrData(data) {
        return Repository.post(`${resource}/getServiceMonitData`, data);
    },
};