import Repository from "./Repository";

const resource = "/tmqa";

export default {
    getQaTargetList(data) {
        return Repository.post(`${resource}/target/list`, data);
    },
    setTargetY(data) {
        return Repository.post(`${resource}/target/targetY`, data);
    },
    setTargetN(data) {
        return Repository.post(`${resource}/target/targetN`, data);
    },


    getTmrUserList() {
        return Repository.get(`${resource}/target/tmr`);
    },
    getAutoScrList() {
        return Repository.get(`${resource}/target/scr`);
    },
    getDtctList() {
        return Repository.get(`${resource}/target/dtct`)
    },
    getProdList() {
        return Repository.get(`${resource}/target/prod`)
    },
    getCount() {
        return Repository.get(`${resource}/target/count`)
    },
    
    setAutoTmrUser(data) {
        return Repository.post(`${resource}/target/tmr`, data);
    },
    setAutoScr(data) {
        return Repository.post(`${resource}/target/scr`, data);
    },
    setAutoDtct(data) {
        return Repository.post(`${resource}/target/dtct`, data);
    },
    setAutoProd(data) {
        return Repository.post(`${resource}/target/prod`, data);
    },
    setAutoCnt(data) {
        return Repository.post(`${resource}/target/count`, data);
    },
};