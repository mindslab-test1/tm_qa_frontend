import Repository from "./Repository";

const resource = "/manage";

export default {
    // 표준스크립트 관리
    getProductList(data) {
        return Repository.post(`${resource}/standardScript/prodlist`, data);
    },
    getEvalList(data) {
        return Repository.post(`${resource}/standardScript/evaluationItem`, data);
    },
    getIntervalList(data) {
        return Repository.post(`${resource}/standardScript/intervalChoice`, data);
    },
    getSubguideList(data) {
        return Repository.post(`${resource}/standardScript/scriptSubList`, data);
    },
    addProductList(data) {
        return Repository.post(`${resource}/standardScript/proc-product`, data);
    },
    copyProduct(data) {
        return Repository.post(`${resource}/standardScript/proc-copy`, data);
    },
    getEvalModalProdCatList(data) {
        return Repository.post(`${resource}/standardScript/productcode`, data);
    },
    getEvalModalAppOrdList(data) {
        return Repository.post(`${resource}/standardScript/applyordlist`, data);
    },
    getEvalModalMainList(data) {
        return Repository.post(`${resource}/standardScript/prodcatlist`, data);
    },
    getEvalModalDetailList(data) {
        return Repository.post(`${resource}/standardScript/prodcat-detail-list`, data)
    },
    addEvalItem(data) {
        return Repository.post(`${resource}/standardScript/insert-checkitem`, data)
    },
    deleteEvalItem(data) {
        return Repository.post(`${resource}/standardScript/del-checkitem`, data)
    },
    getScriptLctgList() {
        return Repository.get(`${resource}/standardScript/script-lctg`)
    },
    getScriptMctgList() {
        return Repository.get(`${resource}/standardScript/script-mctg`)
    },
    getScriptSctgList() {
        return Repository.get(`${resource}/standardScript/script-sctg`)
    },
    getScriptMainList(data) {
        return Repository.post(`${resource}/standardScript/script-mainlist`, data)
    },
    getScriptDetailList(data) {
        return Repository.post(`${resource}/standardScript/script-detaillist`, data)
    },
    addScrpSecInfo(data) {
        return Repository.post(`${resource}/standardScript/add-scrpsecinfo`, data)
    },
    deleteScrpSecInfo(data) {
        return Repository.post(`${resource}/standardScript/del-scrpsecinfo`, data)
    },
    // 답변 관리
    getAnswerMainList(data) {
        return Repository.post(`${resource}/ansManageApi/mainList`, data);
    },
    addAns(data) {
        return Repository.post(`${resource}/ansManageApi/addAns`, data);
    },
    addAnsList(data) {
        return Repository.post(`${resource}/ansManageApi/addAnsList`, data);
    },
    getAnsEditList(data) {
        return Repository.post(`${resource}/ansManageApi/getAnsEditList`, data);
    },
    deleteAns(data) {
        return Repository.post(`${resource}/ansManageApi/delEditAnsMnConItem`, data);
    },
    deleteAnsList(data) {
        return Repository.post(`${resource}/ansManageApi/delContList`, data);
    },
    editAns(data) {
        return Repository.post(`${resource}/ansManageApi/saveEditAnsNmItem`, data);
    },
    editAnsList(data) {
        return Repository.post(`${resource}/ansManageApi/saveEditAnsContItem`, data);
    },
    // 점검항목 관리
    getProdCatList(data) {
        return Repository.post(`${resource}/evaluationApi/getProductCode`, data);
    },
    getProdOrdList(data) {
        return Repository.post(`${resource}/evaluationApi/applyOrdList`, data);
    },
    getEvalMainList(data) {
        return Repository.post(`${resource}/evaluationApi/getMainList`, data);
    },
    getEvalSubList(data) {
        return Repository.post(`${resource}/evaluationApi/getSubList`, data);
    },
    getPopupProductList(data) {
        return Repository.post(`${resource}/evaluationApi/popProductList`, data);
    },
    addPopupProductList(data) {
        return Repository.post(`${resource}/evaluationApi/addProductCode`, data);
    },
    deletePopupProductList(data) {
        return Repository.post(`${resource}/evaluationApi/delProductCode`, data);
    },
    procPopupApplyOrd(data) {
        return Repository.post(`${resource}/evaluationApi/procApplyOrd`, data);
    },
    getEvalCondList(data) {
        return Repository.post(`${resource}/evaluationApi/getCondList`, data);
    },
    addCheckItem(data) {
        return Repository.post(`${resource}/evaluationApi/addCheckItem`, data);
    },
    deleteCheckItem(data) {
        return Repository.post(`${resource}/evaluationApi/delCheckItem`, data);
    },
    getEvalCategoryList(data) {
        return Repository.post(`${resource}/evaluationApi/getEvalCategoryList`, data);
    },
    // 문장 관리
    getSentenceMainList(data) {
        return Repository.post(`${resource}/sentenceManageApi/loadSentenceManageList`, data);
    },
    getAddPopupList(data) {
        return Repository.post(`${resource}/sentenceManageApi/getApplyAnswer`, data);
    },
    addSentenceList(data) {
        return Repository.post(`${resource}/sentenceManageApi/addSentenceList`, data);
    },
    getSentenceDetail(data) {
        return Repository.post(`${resource}/sentenceManageApi/getSentenceDetail`, data);
    },
    getSentencePopupList(data) {
        return Repository.post(`${resource}/sentenceManageApi/getSentencePopList`, data);
    },
    deleteSentenceDetail(data) {
        return Repository.post(`${resource}/sentenceManageApi/delSentenceDetail`, data);
    },
    updateSentenceDetail(data) {
        return Repository.post(`${resource}/sentenceManageApi/updateSentenceData`, data);
    },
    getSentenceDtcList(data) {
        return Repository.post(`${resource}/sentenceManageApi/getSentenceDtcList`, data);
    },
    deleteSentenceDtcDtlInfo(data) {
        return Repository.post(`${resource}/sentenceManageApi/delDtcDtlInfo`, data);
    },
    addSentenceDtcDtlInfo(data) {
        return Repository.post(`${resource}/sentenceManageApi/insertDtcDtlInfo`, data);
    },
	updateDtcDtl(data) {
		return Repository.post(`${resource}/sentenceManageApi/updDtcDtl`, data);
	},
    // 권한설정
    getUsers(data) {
        return Repository.get(`${resource}/user/users?` + Object.keys(data).map(k => `${k}=${encodeURIComponent(data[k])}`).join('&'));
    },
    getUser(userId) {
        return Repository.get(`${resource}/user/${userId}`);
    },
    getAuths() {
        return Repository.get(`${resource}/user/auths`);
    },
    setUser(data) {
        return Repository.post(`${resource}/user`, data);
    },
    updUser(data) {
        return Repository.patch(`${resource}/user`, data);
    },
    updUnlockUser(userId) {
        return Repository.patch(`${resource}/user/${userId}`);
    },
    delUser(userId) {
        return Repository.delete(`${resource}/user/${userId}`);
    },
    // TA 재처리
    getTaReProgressStatusCode() {
        return Repository.get(`${resource}/taReprocess/progStatCd`)
    },
    getTaReMainList(data) {
        return Repository.post(`${resource}/taReprocess/taReMainList`, data)
    },
    getTaReTotalCount(data) {
        return Repository.post(`${resource}/taReprocess/taReProcessTotalCnt`, data)
    },
    getTaReGroupCount(data) {
        return Repository.post(`${resource}/taReprocess/taReGroupCnt`, data)
    },
    updateTaReprocess(data) {
        return Repository.post(`${resource}/taReprocess/updateReProcess`, data)
    },
    // 상담구간 관리
    getConsultSections(data) {
        return Repository.get(`${resource}/consult/sections?` + Object.keys(data).map(k => `${k}=${encodeURIComponent(data[k])}`).join('&'));
    },
    getConsultSentences(cuslCd, cuslAppOrd) {
        return Repository.get(`${resource}/consult/section/${cuslCd}/${cuslAppOrd}/sentences`);
    },
    getConsultCodes() {
        return Repository.get(`${resource}/consult/codes`);
    },
    setConsultSection(data) {
        return Repository.post(`${resource}/consult/section`, data);
    },
    updConsultSection(data) {
        return Repository.patch(`${resource}/consult/section`, data);
    },
    delConsultSection(cuslCd, cuslAppOrd) {
        return Repository.delete(`${resource}/consult/section/${cuslCd}-${cuslAppOrd}`);
    },
    getSentences(data) {
        return Repository.get(`${resource}/consult/sentences?` + Object.keys(data || []).map(k => `${k}=${encodeURIComponent(data[k])}`).join('&'));
    },
    setConsultSectionSentences(cuslCd, cuslAppOrd, data) {
        return Repository.post(`${resource}/consult/section/${cuslCd}-${cuslAppOrd}/sentences`, data);
    },
    delConsultSectionSentences(cuslCd, cuslAppOrd, data) {
        return Repository.delete(`${resource}/consult/section/${cuslCd}-${cuslAppOrd}/sentences`, data);
    },
    // 시뮬레이션
    getSimulationNlp(data) {
        return Repository.post(`${resource}/simulation/callNlp`, data);
    },
    getSimulationHmd(data) {
        return Repository.post(`${resource}/simulation/callHmd`, data);
    },
    // 문장발화관리
    getConditionProdCatList() {
        return Repository.get(`${resource}/puncStc/getProdCat`);
    },
    getConditionMainList(data) {
        return Repository.post(`${resource}/puncStc/getMainList`, data);
    },
    addConditionItem(data) {
        return Repository.post(`${resource}/puncStc/insertCondItem`, data);
    },
    deleteConditionItem(data) {
        return Repository.post(`${resource}/puncStc/deleteCondItem`, data);
    },
    editConditionItem(data) {
        return Repository.post(`${resource}/puncStc/editCondItem`, data);
    },
    getSpecialAgList(data) {
        return Repository.post(`${resource}/puncStc/getSpecialAgList`, data);
    },
    addSpecialAgList(data) {
        return Repository.post(`${resource}/puncStc/addSpecialAgList`, data);
    },
    getPuncList(data) {
        return Repository.post(`${resource}/puncStc/getPuncList`, data);
    },
    addSpChkItem(data) {
        return Repository.post(`${resource}/puncStc/addSpChkItem`, data);
    },
    deleteSpChkItem(data) {
        return Repository.post(`${resource}/puncStc/delSpChkItem`, data);
    },

    // 스크립트 구간 관리
    setCode(data) {
        return Repository.post(`${resource}/scriptAreaApi/code`, data);
    },
    getLctgList() {
        return Repository.get(`${resource}/scriptAreaApi/lctgList`);
    },
    getCodeList(data) {
        return Repository.get(`${resource}/scriptAreaApi/codeList?${data}`);
    },
    updCode(data) {
        return Repository.put(`${resource}/scriptAreaApi/code`, data);
    },
    delCode(data) {
        return Repository.delete(`${resource}/scriptAreaApi/code`, { data: data });
    },
    getScriptAreaList(data) {
        return Repository.get(`${resource}/scriptAreaApi/scriptAreaList?${data}`);
    },
    setScriptArea(data) {
        return Repository.post(`${resource}/scriptAreaApi/scriptArea`, data);
    },
    updScriptArea(data) {
        return Repository.put(`${resource}/scriptAreaApi/scriptArea`, data);
    },
    delScriptArea(data) {
        return Repository.delete(`${resource}/scriptAreaApi/scriptArea?${data}`);
    },
    getScriptAreaMainList(data) {
        return Repository.get(`${resource}/scriptAreaApi/scriptAreaMainList?${data}`);
    },
    getScriptSentenceList(data) {
        return Repository.get(`${resource}/scriptAreaApi/scriptSentenceList?${data}`);
    },
    getSentenceList(data) {
        return Repository.get(`${resource}/scriptAreaApi/sentenceList?${data}`);
    },
    setScriptSentence(data) {
        return Repository.post(`${resource}/scriptAreaApi/scriptSentence`, data);
    },
    delScriptSentence(data) {
        return Repository.delete(`${resource}/scriptAreaApi/scriptSentence`, { data: data });
    },
	getArticleDetail(data) {
		return Repository.get(`${resource}/scriptAreaApi/articleDetail?${data}`);
	},
	getCondList() {
		return Repository.get(`${resource}/scriptAreaApi/condList`);
	},
	getCondDetail(data) {
		return Repository.get(`${resource}/scriptAreaApi/condDetail?${data}`);
	},
	updArticle(data) {
		return Repository.post(`${resource}/scriptAreaApi/article`, data);
	},

	// 권한 관리
	getAuthList(data) {
		return Repository.get(`${resource}/authList?${data}`);
	},
	setAuth(data) {
		return Repository.post(`${resource}/auth`, data);
	},
	getAuth(data) {
		return Repository.get(`${resource}/auth?${data}`);
	},
	updAuth(data) {
		return Repository.put(`${resource}/auth`, data);
	},
	delAuth(data) {
		return Repository.delete(`${resource}/auth`, {data: data});
	},
	getAccessibleMenuList(data) {
		return Repository.get(`${resource}/auth/menuList?${data}`);
	},
    getMenuList() {
        return Repository.get(`${resource}/auth/menu`);
    },
	setAccessibleMenu(data) {
		return Repository.post(`${resource}/auth/menu`, data);
	},
	delAccessibleMenu(data) {
		return Repository.delete(`${resource}/auth/menu`, {data: data});
	},
};