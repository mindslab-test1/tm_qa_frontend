import Repository from "./Repository";

const resource = "/tmqa";

export default {
    getTmqaAllSttRstInfo(data) {
        return Repository.get(`${resource}/allSttRstInfo?${data}`);
    },
    getTmqaAllSttRstList(data) {
        return Repository.get(`${resource}/allSttRst/list?${data}`);
    },
    updTmSttStmt(data) {
        return Repository.put(`${resource}/allSttRst/stmt`, data);
    },

};