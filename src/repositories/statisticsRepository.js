import Repository from "./Repository";

const resource = "/statistics";

export default {
    getQaSelectBoxList() {
        return Repository.get(`${resource}/getQaList`);
    },
    getQaStatMainList(data) {
        return Repository.post(`${resource}/getQaStatMainList`, data)
    },
    getTmrStatMainList(data) {
        return Repository.post(`${resource}/getTmrStatMainList`, data)
    },
};