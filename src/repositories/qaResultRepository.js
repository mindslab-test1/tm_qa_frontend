import Repository from "./Repository";

const resource = "/tmqaResult";

export default {
    getQaResultList(data) {
        return Repository.post(`${resource}/getQaResultList`, data);
    },
    getQaResultExcel(data) {
        return Repository.post(`${resource}/qaResultExcelDown`, data);
    },
};