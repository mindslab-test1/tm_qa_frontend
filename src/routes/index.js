import Vue from 'vue';
import Router from 'vue-router';
import store from '../store/index';
import { RepositoryFactory } from '@/repositories/RepositoryFactory';

const UserRepository = RepositoryFactory.get('user');

Vue.use(Router);

const alreadyLoggedRouting = async (to, from, next) => {
    const userString = localStorage.getItem('access_token');
    if (userString) {
        try {
            let { data } = await UserRepository.me();
            let url = '';
            switch (data.userRole) {
                case "ROLE_ADMIN":
                case "ROLE_QA_ADMIN":
                case "ROLE_UW_ADMIN": url = '/main/dashboard'; break;
                case "ROLE_QA_REVIEWER":
                case "ROLE_UW_REVIEWER": url = '/qa/assign/reviewer'; break;
                case "ROLE_TM_CENTER": url = '/qa/result'; break;
                default: url = '/main/dashboard';
            }
            return next(url);
        } catch (err) {
            console.log(err)
        }
    }

    next();
};

const checkForUserAuth = async (to, from, next) => {
    let loggedIn = false;
    try {
        let { data } = await UserRepository.me();
        store.commit('user/setUserInfo', data);
        loggedIn = true;
        localStorage.setItem("access_token", data.accessToken);
        store.commit('user/clearTokenInterval');
        await store.dispatch('user/checkTokenTimeOut', data.expiredTime);
    } catch (err) {
        console.log(err)
    }

    if (loggedIn == false) {
        store.commit('user/clearUserInfo');
        store.commit('user/clearTokenInterval');
        /* alert('로그인이 필요한 기능입니다.') */
        return next({
            name: 'Login',
            params: {
                next: to.fullPath
            },
        });
    }

    const {data: pageAuthCheck} = await UserRepository.pageAuthCheck(to.path);
    if(!pageAuthCheck){
        alert('페이지 접근 권한이 없습니다.');
        return next({
            name: 'Login'
        });
    }

    const key = new Date().getMilliseconds();
    if (store.getters["base/keepComponents"].length > 0) {
        if (!to.query.key) {
            store.commit('base/setComponent', { routeName: to.name, key: `${to.name}-${key}`, to: `${to.fullPath}?key=${key}`, korName: to.meta.korName });
            to.params.key = `${to.name}-${new Date().getMilliseconds()}`;
        } else {

        }
    } else {
        if (to.query.key) {
            store.commit('base/setComponent', { routeName: to.name, key: `${to.name}-${key}`, to: to.fullPath, korName: to.meta.korName });
        } else {
            store.commit('base/setComponent', { routeName: to.name, key: `${to.name}-${key}`, to: `${to.fullPath}?key=${key}`, korName: to.meta.korName });
            to.params.key = `${to.name}-${key}`;
        }
    }
    next()
};

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'Login',
        beforeEnter: alreadyLoggedRouting,
        component: require('@/components/login/Login').default,
        meta: {
            title: 'TMQA - LOGIN',
            layout: 'LoginLayout',
        }
    },
    // realtime
    {
        path: '/real-time/dashboard',
        name: 'RealtimeDashboard',
        beforeEnter: checkForUserAuth,
        component: require('@/components/realtime/RealtimeDashboard').default,
        meta: {
            title: 'TMQA - REAL-TIME - DASHBOARD',
            location: 'REAL-TIME - DASHBOARD',
            menu: 'REALTIME',
            korName: 'REAL-TIME DASHBOARD'
        }
    },
    {
        path: '/main/dashboard',
        name: 'Dashboard',
        beforeEnter: checkForUserAuth,
        component: require('@/components/main/Dashboard').default,
        meta: {
            title: 'TMQA - MAIN - DASHBOARD',
            location: 'MAIN - DASHBOARD',
            menu: 'HOME',
            korName: 'Dashboard'
        }
    },
    // POPUP
    {
        path: '/popup/tm/stt/target/stt-all',
        name: 'PopupTmSttAll',
        beforeEnter: checkForUserAuth,
        component: require('@/components/popup/tm/stt/target/TmSttAllPopup').default,
        meta: {
            title: 'TMQA - TM - STT-ALL',
            layout: 'PopupLayout',
        }
    },
    {
        path: '/popup/tm/qa/target/set',
        name: 'PopupTmQaTargetSet',
        beforeEnter: checkForUserAuth,
        component: require('@/components/popup/tm/qa/target/TmQaTargetSetPopup').default,
        meta: {
            title: 'TMQA - TM_QA - TARGET - SET',
            layout: 'PopupLayout',
        }
    },
    {
        path: '/popup/tm/qa/audit/stt-all',
        name: 'PopupTmQaAuditSttAll',
        beforeEnter: checkForUserAuth,
        component: require('@/components/popup/tm/qa/audit/TmQaAuditSttAllPopup').default,
        meta: {
            title: 'TMQA - TM_QA - AUDIT - STT-ALL',
            layout: 'PopupLayout',
        }
    },
    {
        path: '/popup/qa/audit',
        name: 'PopupTmQaAudit',
        beforeEnter: checkForUserAuth,
        component: require('@/components/popup/tm/qa/audit').default,
        meta: {
            title: 'TMQA - TM_QA - AUDIT',
            layout: 'PopupLayout',
        }
    },
    {
        path: '/popup/standardscript/sentence/settings',
        name: 'PopupStandardScriptSentenceSettings',
        beforeEnter: checkForUserAuth,
        component: require('@/components/popup/manage/standardscript/StandardScriptSentenceSettingsPopup').default,
        meta: {
            title: 'STANDARD SCRIPT - SENTENCE - SETTINGS',
            layout: 'PopupLayout',
        }
    },

    // cs, qa
    {
        path: '/tm/stt/target',
        name: 'TmSttTarget',
        beforeEnter: checkForUserAuth,
        component: require('@/components/tm/stt/target').default,
        meta: {
            title: 'TMQA - CS - STT TARGET',
            location: 'CS - STT TARGET',
            menu: 'CS',
            korName: 'TM - STT 대상'
        }
    },
    {
        path: '/qa/target',
        name: 'QaTarget',
        beforeEnter: checkForUserAuth,
        component: require('@/components/qa/target').default,
        meta: {
            title: 'TMQA - QA TARGET',
            location: 'TMQA - QA TARGET',
            menu: 'TM_QA',
            korName: 'QA 대상'
        }
    },
    {
        path: '/qa/assign/admin',
        name: 'QaAssignAdmin',
        beforeEnter: checkForUserAuth,
        component: require('@/components/qa/assign/admin').default,
        meta: {
            title: 'TMQA - QA ASSIGN - ADMIN',
            location: 'TMQA - QA ASSIGN - ADMIN',
            menu: 'TM_QA',
            korName: 'QA 배정 - 관리'
        }
    },
    {
        path: '/qa/assign/reviewer',
        name: 'QaAssignReviewer',
        beforeEnter: checkForUserAuth,
        component: require('@/components/qa/assign/reviewer').default,
        meta: {
            title: 'TMQA - QA ASSIGN - REVIEWER',
            location: 'TMQA - QA ASSIGN - REVIEWER',
            menu: 'TM_QA',
            korName: 'QA 배정 - 심사'
        }
    },
    {
        path: '/qa/result',
        name: 'QaResult',
        beforeEnter: checkForUserAuth,
        component: require('@/components/qa/result').default,
        meta: {
            title: 'TMQA - QA RESULT',
            location: 'TMQA - QA RESULT',
            menu: 'TM_QA',
            korName: 'QA 결과'
        }
    },

    // statistics
    {
        path: '/statistics/reviewer',
        name: 'StatisticsReviewer',
        beforeEnter: checkForUserAuth,
        component: require('@/components/statistics/Reviewer').default,
        meta: {
            title: 'TMQA - STATISTICS - Reviewer',
            location: 'STATISTICS - REVIEWER',
            menu: 'STATISTICS',
            korName: '심사자 통계'
        }
    },
    {
        path: '/statistics/consultant',
        name: 'StatisticsConsultant',
        beforeEnter: checkForUserAuth,
        component: require('@/components/statistics/Consultant').default,
        meta: {
            title: 'TMQA - STATISTICS - Consultant',
            location: 'STATISTICS - CONSULTANT',
            menu: 'STATISTICS',
            korName: '상담원 통계'
        }
    },

    // monitoring
    {
        path: '/monitoring/procstatus',
        name: 'ProcStatusMonitoring',
        beforeEnter: checkForUserAuth,
        component: require('@/components/monitoring/ProcStatusMonitoring').default,
        meta: {
            title: 'TMQA - MONITORING - PROCESS STATUS MONITORING',
            location: 'MONITORING - PROCESS STATUS MONITORING',
            menu: 'MONITORING',
            korName: '처리 상태 모니터링'
        }
    },
    {
        path: '/monitoring/service',
        name: 'ServiceMonitoring',
        beforeEnter: checkForUserAuth,
        component: require('@/components/monitoring/ServiceMonitoring').default,
        meta: {
            title: 'TMQA - MONITORING - SERVICE MONITORING',
            location: 'MONITORING - SERVICE MONITORING',
            menu: 'MONITORING',
            korName: '서비스 모니터링'
        }
    },

    // manage
    {
        path: '/manage/script-area',
        name: 'ManageScriptArea',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/ScriptArea').default,
        meta: {
            title: 'TMQA - MANAGE - SCRIPT AREA',
            location: 'MANAGE - SCRIPT AREA',
            menu: 'ADMIN',
            korName: '스크립트 구간 관리'
        }
    },
    {
        path: '/manage/standardscript',
        name: 'ManageStandardScript',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/StandardScript').default,
        meta: {
            title: 'TMQA - MANAGE - STANDARD SCRIPT',
            location: 'MANAGE - STANDARD SCRIPT',
            menu: 'ADMIN',
            korName: '표준 스크립트 관리'
        }
    },
    {
        path: '/manage/evaluation',
        name: 'ManageEvaluation',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/Evaluation').default,
        meta: {
            title: 'TMQA - MANAGE - EVALUATION',
            location: 'MANAGE - EVALUATION',
            menu: 'ADMIN',
            korName: '점검 항목 관리'
        }
    },
    {
        path: '/manage/condition',
        name: 'ManageCondition',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/Condition').default,
        meta: {
            title: 'TMQA - MANAGE - CONDITION',
            location: 'MANAGE - CONDITION',
            menu: 'ADMIN',
            korName: '문장 발화 조건'
        }
    },
    {
        path: '/manage/answer',
        name: 'ManageAnswer',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/Answer').default,
        meta: {
            title: 'TMQA - MANAGE - ANSWER',
            location: 'MANAGE - ANSWER',
            menu: 'ADMIN',
            korName: '답변 관리'
        }
    },
    {
        path: '/manage/sentence',
        name: 'ManageSentence',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/Sentence').default,
        meta: {
            title: 'TMQA - MANAGE - SENTENCE',
            location: 'MANAGE - SENTENCE',
            menu: 'ADMIN',
            korName: '문장 관리'
        }
    },
	{
		path: '/manage/user',
		name: 'ManageUser',
		beforeEnter: checkForUserAuth,
		component: require('@/components/manage/User').default,
		meta: {
			title: 'TMQA - MANAGE - USER',
			location: 'MANAGE - USER',
			menu: 'ADMIN',
			korName: '사용자 관리'
		}
	},
	{
		path: '/manage/auth',
		name: 'ManageAuth',
		beforeEnter: checkForUserAuth,
		component: require('@/components/manage/Auth').default,
		meta: {
			title: 'TMQA - MANAGE - AUTH',
			location: 'MANAGE - AUTH',
			menu: 'ADMIN',
			korName: '권한 관리'
		}
	},
    {
        path: '/manage/consult',
        name: 'ManageConsult',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/Consult').default,
        meta: {
            title: 'TMQA - MANAGE - CONSULT',
            location: 'MANAGE - CONSULT',
            menu: 'ADMIN',
            korName: '상담구간 관리'
        }
    },
    {
        path: '/manage/simulation',
        name: 'Simulation',
        beforeEnter: checkForUserAuth,
        component: require('@/components/manage/Simulation').default,
        meta: {
            title: 'TMQA - MANAGE - SIMULATION',
            location: 'MANAGE - SIMULATION',
            menu: 'ADMIN',
            korName: '시뮬레이션 화면'
        }
    },
	{
		path: '/manage/tareprocess',
		name: 'TaReprocess',
		beforeEnter: checkForUserAuth,
		component: require('@/components/manage/TaReprocess').default,
		meta: {
			title: 'TMQA - MANAGE - TA REPROCESS',
			location: 'MANAGE - TA REPROCESS',
			menu: 'ADMIN',
			korName: 'TA 재처리'
		}
	},
	{
		path: '*',
		name: 'notFound',
		redirect: '/'
	},
];
const router = new Router({
    mode: 'history',
    routes: routes
});

const DEFAULT_TITLE = 'TMQA';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;